## Heading 2
### Heading 3
#### Heading 4

----

**bold**
_itlaic_
***bold and italic***

## Links:

[Text to display](link) is for inline links
[Text to display](link){:target="_blank"} is for inline links that open in a new tab

[example@gitlab.com](mailto:example@gitlab.com) email link

[![Semantic description of image](/images/path/to/folder/image.png "Hello World")*My caption*][about.gitlab.com]

## Code Blocks:

`Inline Code Block`
```Python
print('Multiline')
print('Code Block')
```

## Lists:

1. Item 1
2. Item 2
    1. Sub-Item 1
    2. Sub-Item 2

- Item 1
- Item 2
    - Sub-item 1
    - Sub Item-2

- [X] Incomplete Task
- [ ] Completed Task
    - [X] Sub-Task 1
    - [ ] Sub-Task 2

## Tables:

| Header 1 | Header 2 | Header 3|
| ---      | ---      | ---     |
| cell 1   | cell 2   | cell 3  |

[mermaid diagramming in Gitlab](https://docs.gitlab.com/ee/user/markdown.html#diagrams-and-flowcharts){:target="_blank"}