sudo apt-add-repository ppa:gns3/ppa -y
sudo apt-add-repository ppa:lutris-team/lutris -y
sudo apt-add-repository ppa:obsproject/obs-studio -y
sudo apt update
mkdir ~/.local/bin
mkdir ~/.fonts
cp -r ./FiraCode ~/.fonts
sh -c "$(curl -fsSL https://starship.rs/install.sh)"
echo 'eval "$(starship init bash)"' >> ~/.bashrc
echo "export PATH=$PATH:/home/ean/.local/bin" >> ~/.bashrc
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
sudo mkdir /mnt/NAS
sudo apt install nfs-common tilix python3-pip ffmpeg bpytop python3-setuptools python3-venv pipx cockpit virt-manager steam gns3-gui docker.io lutris obs-studio -y
sudo usermod -aG docker,kvm ean
flatpak install com.jgraph.drawio.desktop com.slack.Slack com.todoist.Todoist us.zoom.Zoom com.vscodium.codium -y
sudo update-alternatives --config x-terminal-emulator
sudo reboot
